# Design Principle Acronyms

A short talk on popular acronyms in software development, with some code examples.


## Run

Open index.html in your favorite web browser.

## Credits

Based on [reveal.js](https://github.com/hakimel/reveal.js) presentation framework.
